package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductCreateRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    /**
     * @param sku
     * @return product response
     */
    @GetMapping("/product/{sku}", produces = ["application/json"])
    fun findProductsBySku(
            @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService?.findProductBySku(sku)

        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    /**
     * @param product
     * @return newly created product
     */
    @PostMapping("/product", produces = ["application/json"])
    fun addProduct(@RequestBody product:ProductCreateRequest): ResponseEntity<ProductResponse> {
        logger.info("creating product ${product.sku}")
        return ResponseEntity.ok(productService.addProduct(product));
    }

    @PatchMapping("/product", produces=["application/json"])
    fun patchProduct(@RequestBody product:ProductUpdateRequest): ResponseEntity<ProductResponse> {
        logger.info("patching product ${product.sku}")
        return ResponseEntity.ok(productService.patchProduct(product));
    }

    /**
     * @param skuList
     * @return product list or empty if none of skus exists
     */
    @GetMapping("/products", produces = ["application/json"])
    fun findMultipleProducts(@RequestParam(name="skuList")  skuList:List<String>) : ResponseEntity<List<ProductResponse>>{
        logger.info("Request for products in sku list $skuList")
        val productList = productService.findProductsBySkus(skuList)
        return if(productList == null){
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(productList)
        }
    }
}
