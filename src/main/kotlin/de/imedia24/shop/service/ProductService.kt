package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductCreateRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.domain.product.toEntity
import org.springframework.stereotype.Service
import java.util.NoSuchElementException

/**
 * represents the Service Layer for the Product Repository
 */
@Service
class ProductService(private val productRepository: ProductRepository) {

    /**
     * takes a product sku
     * @return a ProductResponse
     */
    fun findProductBySku(sku: String): ProductResponse? {
        val product = productRepository?.findBySku(sku)
        return product?.toProductResponse()
    }

    /**
     * takes a list of product skus
     * @return a list of ProductResponse
     */
    fun findProductsBySkus(skus:List<String>): List<ProductResponse>? {
        val products = productRepository?.findAllBySkuIn(skus)
        return products?.map{ product -> product.toProductResponse()}
    }

    fun addProduct(product: ProductCreateRequest): ProductResponse {
        return productRepository.save(product.toEntity()).toProductResponse()
    }

    fun patchProduct(requestProduct:ProductUpdateRequest): ProductResponse {
        val product = productRepository.findBySku(requestProduct.sku)?: throw NoSuchElementException("could not find product width sku number ${requestProduct.sku}");
        val updatedProduct = productRepository.save(requestProduct.toEntity(product));
        return updatedProduct.toProductResponse();
    }
}
