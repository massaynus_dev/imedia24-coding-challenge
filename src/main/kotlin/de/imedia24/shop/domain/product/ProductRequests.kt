package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductUpdateRequest (
    val sku:String,
    val name: String,
    val description: String,
    val price: BigDecimal,
        )

fun ProductUpdateRequest.toEntity(product:ProductEntity) = ProductEntity(
    sku = product.sku,
    name= name,
    description = description,
    price = price,
    stock = product.stock,
    createdAt = product.createdAt,
    updatedAt = ZonedDateTime.now(),

)
data class ProductCreateRequest(
    val sku:String,
    val name: String,
    val description: String,
    val price: BigDecimal,
    val stock: Int
)

fun ProductCreateRequest.toEntity() = ProductEntity(
    sku = sku,
    name= name,
    description = description,
    price = price,
    stock = stock,
    createdAt = ZonedDateTime.now(),
    updatedAt = ZonedDateTime.now()
)