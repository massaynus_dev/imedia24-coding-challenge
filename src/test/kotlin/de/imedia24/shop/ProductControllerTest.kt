package de.imedia24.shop

import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.service.ProductService
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import com.ninjasquad.springmockk.MockkBean
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.domain.product.ProductUpdateRequest
import de.imedia24.shop.domain.product.toEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal
import java.time.ZonedDateTime

@WebMvcTest
class ProductControllerTest (@Autowired val mockMvc:MockMvc,@Autowired val objectMapper: ObjectMapper){
    @MockkBean
    lateinit var productService : ProductService

    @Test
    fun `should return the product with the given sku`() {
        val product = ProductResponse("1","apple tv","smart tv", BigDecimal(1000),12)
        every{productService.findProductBySku(product.sku)} returns product
        mockMvc.perform(get("/product/${product.sku}").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("\$.name").value(product.name))
    }

    @Test
    fun `should partially update a product`(){

       val updatedProduct = ProductUpdateRequest("1","apple tv","smart tv", BigDecimal(1000));
        val product = ProductEntity("1","apple tv","smart tv", BigDecimal(1000),13, ZonedDateTime.now(), ZonedDateTime.now());
        every{productService.patchProduct(updatedProduct)} returns updatedProduct.toEntity(product).toProductResponse()

        mockMvc.perform(patch("/product").contentType(MediaType.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(updatedProduct)))
            .andExpect(status().isOk)
            .andExpect(content().json(objectMapper.writeValueAsString(updatedProduct)))
            .andDo(print());
    }
}